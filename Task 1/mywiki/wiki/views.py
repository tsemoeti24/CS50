from django.shortcuts import render, redirect
from .models import Article
from .forms import ArticleForm, CommentForm
from django.contrib.auth.decorators import login_required

def article_list(request):
    articles = Article.objects.all()
    return render(request, 'wiki/article_list.html', {'articles': articles})

def article_detail(request, article_id):
    article = Article.objects.get(pk=article_id)
    comments = article.comments.all()
    comment_form = CommentForm()

    if request.method == 'POST':
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.article = article
            new_comment.author = request.user
            new_comment.save()
            return redirect('article_detail', article_id=article_id)

    return render(request, 'wiki/article_detail.html', {'article': article, 'comments': comments, 'comment_form': comment_form})

@login_required
def article_create(request):
    if request.method == 'POST':
        form = ArticleForm(request.POST)
        if form.is_valid():
            new_article = form.save(commit=False)
            new_article.created_by = request.user
            new_article.save()
            return redirect('article_detail', article_id=new_article.pk)
    else:
        form = ArticleForm()

    return render(request, 'wiki/article_create.html', {'form': form})

@login_required
def article_edit(request, article_id):
    article = Article.objects.get(pk=article_id)

    if request.user != article.created_by:
        return redirect('article_detail', article_id=article_id)

    if request.method == 'POST':
        form = ArticleForm(request.POST, instance=article)
        if form.is_valid():
            form.save()
            return redirect('article_detail', article_id=article_id)
    else:
        form = ArticleForm(instance=article)

    return render(request, 'wiki/article_edit.html', {'form': form, 'article': article})
