from django.shortcuts import render, redirect, get_object_or_404
from .models import Listing, Bid, Watchlist
from .forms import ListingForm, BidForm

def index(request):
    # Display a list of active listings
    listings = Listing.objects.filter(active=True)
    return render(request, 'auctions/index.html', {'listings': listings})

def create_listing(request):
    # Create a new listing
    if request.method == 'POST':
        form = ListingForm(request.POST)
        if form.is_valid():
            new_listing = form.save(commit=False)
            new_listing.seller = request.user
            new_listing.save()
            return redirect('index')
    else:
        form = ListingForm()
    return render(request, 'auctions/create_listing.html', {'form': form})

def listing_detail(request, listing_id):
    # View details of a listing and allow placing bids
    listing = get_object_or_404(Listing, id=listing_id)
    bids = Bid.objects.filter(listing=listing)
    if request.method == 'POST':
        form = BidForm(request.POST)
        if form.is_valid():
            bid = form.save(commit=False)
            bid.bidder = request.user
            bid.listing = listing
            bid.save()
    else:
        form = BidForm()
    return render(request, 'auctions/listing_detail.html', {'listing': listing, 'bids': bids, 'form': form})

def watchlist(request):
    # Display and manage a user's watchlist
    watchlist_items = Watchlist.objects.filter(user=request.user)
    return render(request, 'auctions/watchlist.html', {'watchlist_items': watchlist_items})

def add_to_watchlist(request, listing_id):
    # Add a listing to the user's watchlist
    listing = get_object_or_404(Listing, id=listing_id)
    watchlist, created = Watchlist.objects.get_or_create(user=request.user)
    watchlist.listings.add(listing)
    return redirect('watchlist')

def remove_from_watchlist(request, listing_id):
    # Remove a listing from the user's watchlist
    listing = get_object_or_404(Listing, id=listing_id)
    watchlist = Watchlist.objects.get(user=request.user)
    watchlist.listings.remove(listing)
    return redirect('watchlist')
