# email_client/forms.py

from django import forms
from .models import Email

class EmailForm(forms.ModelForm):
    class Meta:
        model = Email
        fields = ['subject', 'sender', 'recipients', 'message']
