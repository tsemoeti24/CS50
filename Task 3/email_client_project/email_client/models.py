from django.db import models

class Email(models.Model):
    subject = models.CharField(max_length=255)
    sender = models.EmailField()
    recipients = models.TextField()
    message = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
