
from django.shortcuts import render, redirect
from .forms import EmailForm
import requests

EMAIL_API_URL = 'https://your-email-api.com/api/'

def create_email(request):
    if request.method == 'POST':
        form = EmailForm(request.POST)
        if form.is_valid():
            # Send email using API
            api_data = {
                'subject': form.cleaned_data['subject'],
                'sender': form.cleaned_data['sender'],
                'recipients': form.cleaned_data['recipients'],
                'message': form.cleaned_data['message'],
            }
            response = requests.post(EMAIL_API_URL + 'send/', data=api_data)
            if response.status_code == 200:
                return redirect('inbox')  # Redirect to inbox after sending
            else:
                # Handle API error
                form.add_error(None, 'Error sending email. Please try again.')
    else:
        form = EmailForm()

    return render(request, 'email_client/create_email.html', {'form': form})

def inbox(request):
    # Retrieve emails using API
    response = requests.get(EMAIL_API_URL + 'inbox/')
    emails = response.json() if response.status_code == 200 else []

    return render(request, 'email_client/inbox.html', {'emails': emails})
